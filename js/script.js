$(window).ready(function() {
    var obj_fotorama_img = {
        autoplay: "5000",
        loop: true,
        nav: "none",
        arrows: "false",
        click: "false",
        swipe: "false",
        width: "100%",
        ratio: "1800/430"
    };
    var $fotorama = $('.fotorama');
    for(key in obj_fotorama_img) {
        $fotorama.attr("data-"+key,obj_fotorama_img[key]);
    }

    var str_slide_img_1, str_slide_img_2, str_slide_img_3;
    var str_slide_html_1, str_slide_html_2, str_slide_html_3;

    str_slide_img_1 = 'images/iphone.png';
    str_slide_img_2 = 'images/samsung.png';
    str_slide_img_3 = 'images/nokia.png';

    var str_slide_html_1 = str_slide_html_2 = str_slide_html_3 = '<div class="slider-text">' +
        '<h1>Lorem Ipsum is simply</h1>' +
        '<p>dummy text of the printing & typesetting</p></div>';

    $fotorama.fotorama({
        data: [
            {img: str_slide_img_1, html: str_slide_html_1},//
            {img: str_slide_img_2, html: str_slide_html_2},//
            {img: str_slide_img_3, html: str_slide_html_3}//
        ]
    });

});
